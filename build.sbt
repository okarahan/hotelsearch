name := "EpicCompaniesApp"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "joda-time" % "joda-time" % "2.3"
)     

play.Project.playJavaSettings
