package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.HotelDetailNotFoundException;
import exceptions.InvalidDocumentException;
import models.Hotel;
import models.HotelDetail;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Result;
import play.libs.WS.*;

import static play.data.Form.form;
import static play.libs.F.Function;
import static play.libs.F.Promise;

import util.LateRoomsDocParser;
import util.LateRoomsRequestBuilder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.*;

/**
 * Created by okarahan on 20.12.13.
 */
public class HotelController extends Controller {

    /**
     * Returns a hotel list which are bookable between the given dates
     *
     * @param begin
     * @param end
     * @param city
     * @return
     */

    public static Promise<Result> getHotelList(String begin, String end, String city){

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");

        final LocalDate beginDate = formatter.parseLocalDate(begin);
        final LocalDate endDate = formatter.parseLocalDate(end);
        WSRequestHolder request = LateRoomsRequestBuilder.getInstance().buildKeywordRequest(beginDate, endDate, city);

        Promise<WS.Response> homePage = request.get();

         return homePage.map(new Function<WS.Response, Result>() {
            @Override
            public Result apply(WS.Response response) throws Throwable {

                LocalDate now = new LocalDate();

                if(beginDate.isBefore(now) || endDate.isBefore(now)){
                    return internalServerError("One of the selected dates are before now. They have to be in the future.");
                }

                DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource is = new InputSource(new ByteArrayInputStream(response.getBody().getBytes()));

                is.setEncoding("UTF-8");
                Document dom  = docBuilder.parse(is);

                LateRoomsDocParser parser = null;
                try{
                    parser = new LateRoomsDocParser(dom);
                }catch(InvalidDocumentException e){
                    return internalServerError("The XML Service has an error, please contact the admin.");
                }

                List<Hotel> hotels = parser.getHotelList();

                return ok(toJson(hotels));
            }
        });
    }

    /**
     * Returns a HotelDetail with the given id
     *
     * @param id
     * @return
     */

    public static Promise<Result> getHotelDetail(Long id){

        WSRequestHolder request = LateRoomsRequestBuilder.getInstance().buildHotelDetailRequest(id);
        Promise<WS.Response> homePage = request.get();

        return homePage.map(new Function<WS.Response, Result>() {
            @Override
            public Result apply(WS.Response response) throws Throwable {

                DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource is = new InputSource(new ByteArrayInputStream(response.getBody().getBytes()));

                is.setEncoding("UTF-8");
                Document dom = docBuilder.parse(is);

                LateRoomsDocParser parser = null;
                try{
                    parser = new LateRoomsDocParser(dom);
                }catch(InvalidDocumentException e){
                    return internalServerError("The XML Service has an error, please contact the admin.");
                }

                HotelDetail hotelDetail = null;
                try{
                    hotelDetail = parser.getHotelDetail();
                }catch (HotelDetailNotFoundException e){
                    return internalServerError("There could be no details found to the specified hotel.");
                }

                return ok(toJson(hotelDetail));
            }
        });
    }

    private static String toJson(Object object) throws JsonProcessingException{
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}
