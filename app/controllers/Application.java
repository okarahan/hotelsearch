package controllers;

import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import static play.data.Form.form;

public class Application extends Controller {

    public static Result getIndex() {
        return ok(index.render("Willkommen zur Hotelsuche"));
    }

}
