package util;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.Play;
import play.libs.WS;
import play.libs.WS.*;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by okarahan on 07.01.14.
 */
public class LateRoomsRequestBuilder {

    private static LateRoomsRequestBuilder instance = null;
    private Locale locale =  null;

    private final static String
            BASE_URL = "http://xmlfeed.laterooms.com";

    /**
     * Returns instance with german locale
     * @return
     */

    public static LateRoomsRequestBuilder getInstance(){
        return getInstance(Locale.getDefault());
    }

    public static LateRoomsRequestBuilder getInstance(Locale locale){
        if(instance == null){
            instance = new LateRoomsRequestBuilder(locale);
        }
        return instance;
    }

    private LateRoomsRequestBuilder(Locale locale){
        this.locale = locale;
    }

    public void setLocale(Locale locale){
        this.locale = locale;
    }


    /**
     * Builds a request to get all hotels, which are bookable and have the given keyword
     *
     * @param begin
     * @param end
     * @param keyword
     * @return
     */

    public WSRequestHolder buildKeywordRequest(LocalDate begin, LocalDate end, String keyword){

        WSRequestHolder request = getDefaultRequest();
        request.setQueryParameter(QueryParameter.REGION_TYPE, RequestType.KEYWORD_SEARCH);
        request.setQueryParameter(QueryParameter.KEYWORD, keyword);

        DateTimeFormatter usDateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        String beginString = begin.toString(usDateFormatter);

        request.setQueryParameter(QueryParameter.START_DATE, beginString);

        int numberOfNights = calculateNights(begin, end);

        request.setQueryParameter(QueryParameter.NIGHTS, String.valueOf(numberOfNights));

        return request;
    }

    /**
     * Builds the request to get the hotel detail to a given id
     *
     * @param id
     * @return
     */
    public WSRequestHolder buildHotelDetailRequest(Long id){
        WSRequestHolder request = getDefaultRequest();

        request.setQueryParameter(QueryParameter.HOTEL_IDS, String.valueOf(id));
        request.setQueryParameter(QueryParameter.REGION_TYPE, RequestType.HOTEL_DETAILS);

        return request;
    }

    public WSRequestHolder getDefaultRequest(){

        WSRequestHolder request = WS.url(BASE_URL);

        request.setQueryParameter(QueryParameter.AFFILIATE_ID,
                Play.application().configuration().getString("lateroomsApi.aid"));

        request.setQueryParameter(QueryParameter.LANGUAGE, this.locale.getLanguage());
        request.setQueryParameter(QueryParameter.CURRENCY, Currency.getInstance(this.locale).getCurrencyCode());

        return request;

    }

    private static int calculateNights(LocalDate begin, LocalDate end){

        Days days = Days.daysBetween(begin, end);

        return days.getDays();
    }

    // Using constants instead of enums to get directly the string
    private class QueryParameter{
        public static final String AFFILIATE_ID = "aid";
        public static final String LANGUAGE = "lang";
        public static final String CURRENCY = "cur";
        public static final String REGION_TYPE = "rtype";
        public static final String KEYWORD = "kword";
        public static final String START_DATE = "sdate";
        public static final String NIGHTS = "nights";
        public static final String HOTEL_IDS = "hids";
    }

    private class RequestType{
        public static final String HOTEL_DETAILS = "3";
        public static final String KEYWORD_SEARCH = "4";
    }
}
