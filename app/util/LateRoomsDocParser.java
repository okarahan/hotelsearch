package util;

import exceptions.HotelDetailNotFoundException;
import exceptions.InvalidDocumentException;
import models.Hotel;
import models.HotelDetail;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import play.libs.XPath;

import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by okarahan on 13.01.14.
 */
public class LateRoomsDocParser {

    private final Document document;

    public LateRoomsDocParser(Document document) throws InvalidDocumentException {

        Node responseNode = XPath.selectNode("//response", document.getDocumentElement());
        NamedNodeMap attrs = responseNode.getAttributes();
        if(attrs.getNamedItem("status").getNodeValue().equals("0")){
            NodeList errorNodes = XPath.selectNodes("//errors/error", responseNode);
            if(errorNodes.getLength() > 0){
                String errorCode = errorNodes.item(0).getTextContent();
                throw new InvalidDocumentException(errorCode);
            }else{
                throw new InvalidDocumentException();
            }
        }

        this.document = document;
    }

    public List<Hotel> getHotelList(){

        NodeList hotelNodeList = XPath.selectNodes("/root/hotel", this.document.getDocumentElement());
        List<Hotel> hotelList = new ArrayList<>();

        for(int i=0; i< hotelNodeList.getLength(); i++){

            Hotel hotel = new Hotel();

            Node hotelNode = hotelNodeList.item(i);

            hotel.setId(XPath.selectNode("./hotel_ref", hotelNode).getTextContent());
            hotel.setName(XPath.selectNode("./hotel_name", hotelNode).getTextContent());

            hotel.setCity(XPath.selectNode("./hotel_city", hotelNode).getTextContent());

            hotel.setAddress(XPath.selectNode("./hotel_address", hotelNode).getTextContent());

            Node imageNode = XPath.selectNode("./images", hotelNode);
            hotel.setImageUrl(getValidImage(imageNode));

            hotel.setLatitude(XPath.selectNode("./geo_code/lat", hotelNode).getTextContent());
            hotel.setLongitude(XPath.selectNode("./geo_code/long", hotelNode).getTextContent());

            hotelList.add(hotel);
        }

        return hotelList;
    }

    public HotelDetail getHotelDetail() throws HotelDetailNotFoundException{

        NodeList hotelNodes = XPath.selectNodes("/hotel_search/hotel", this.document.getDocumentElement());

        if(hotelNodes.getLength() < 1){
            throw new HotelDetailNotFoundException();
        }

        HotelDetail hotelDetail = new HotelDetail();

        for(int i=0; i< hotelNodes.getLength(); i++){

            Node hotelNode = hotelNodes.item(i);

            hotelDetail.setId(XPath.selectNode("./hotel_ref", hotelNode).getTextContent());
            hotelDetail.setName(XPath.selectNode("./hotel_name", hotelNode).getTextContent());
            hotelDetail.setCity(XPath.selectNode("./hotel_city", hotelNode).getTextContent());
            hotelDetail.setAddress(XPath.selectNode("./hotel_address", hotelNode).getTextContent());
            hotelDetail.setPrice(XPath.selectNode("./prices_from", hotelNode).getTextContent());
            hotelDetail.setCheckIn(XPath.selectNode("./check_in", hotelNode).getTextContent());
            hotelDetail.setCheckOut(XPath.selectNode("./check_out", hotelNode).getTextContent());

            NodeList imageNodes = XPath.selectNodes("./images/url", hotelNode);
            List<String> imageUrls = new ArrayList<>();
            for(int j=0; j < imageNodes.getLength(); j++){
                imageUrls.add(getValidImage(imageNodes.item(j)));
            }
            hotelDetail.setImageUrls(imageUrls);

            hotelDetail.setLatitude(XPath.selectNode("./geo_code/lat", hotelNode).getTextContent());
            hotelDetail.setLongitude(XPath.selectNode("./geo_code/long", hotelNode).getTextContent());

            hotelDetail.setBookingUrl(XPath.selectNode("./hotel_link", hotelNode).getTextContent());
            hotelDetail.setDescription(XPath.selectNode("./hotel_description", hotelNode).getTextContent());

            NodeList facilityNodes = XPath.selectNodes("./facilities/facility", hotelNode);
            List<String> facilities = new ArrayList<String>();
            for(int j=0; j < facilityNodes.getLength(); j++){
                facilities.add(facilityNodes.item(j).getTextContent());
            }
            hotelDetail.setFacilities(facilities);


        }

        return hotelDetail;
    }

    private String getValidImage(Node imageNode){
        String imagePattern = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp)))";

        Pattern pattern = Pattern.compile(imagePattern);
        Matcher matcher = pattern.matcher(imageNode.getTextContent());

        if(matcher.find()){
           return matcher.group();
        }

        return "";
    }
}
