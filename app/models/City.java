package models;

/**
 * Created by okarahan on 13.01.14.
 */
public enum City {
    Berlin("Berlin"),
    NewYork("New York"),
    Paris("Paris");

    private final String name;

    City(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
