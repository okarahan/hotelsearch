package exceptions;

/**
 * Created by okarahan on 14.01.14.
 */
public class InvalidDocumentException extends Exception{

    public InvalidDocumentException(){
        super();
    }

    public InvalidDocumentException(String message){
        super(message);
    }

    public InvalidDocumentException(String message, Throwable cause){
        super(message, cause);
    }
}
