/**
 * Created by okarahan on 20.12.13.
 */


$(function(){
    $('.input-append').datepicker({
        format: 'dd.mm.yyyy'
    });

    $("#form" ).submit(function(e){
        e.preventDefault();

        var city = $("#city" ).val();
        var begin = $("#begin" ).val();
        var end = $("#end" ).val();

        hotelService.getHotels(begin, end, city);
    })
});

markers = new Array();

function printMarkers(hotels){

    if($("#map_canvas").is(":hidden")){
        $("#map_canvas").show();
        $("#map_canvas").css('height', '100%');
        $("#map_canvas").css('width', '100%');
        google.maps.event.trigger(map, 'resize');
    }

    for(var i=0; i<markers.length; i++){
        markers[i].setMap(null);
    }

    markers = new Array();

    var infowindow = new google.maps.InfoWindow();

    $.each(hotels, function(index, hotel){
        var latLng = new google.maps.LatLng(hotel.latitude, hotel.longitude);

        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: hotel.name
        });

        markers.push(marker);

        var contentString = '<div style="width: 200px; height: 200px;" id="content">'+
            '<h4 id="firstHeading" class="firstHeading">'+ hotel.name +'</h4>'+
            '<div id="bodyContent">'+
            '<img style="width: 100px;" src="'+hotel.imageUrl+'"></img>' +
            '<p><a onclick="hotelService.showModal('+hotel.id+')" href="#">'+
            'More Details</a> '+
            '</p>'+
            '</div>'+
            '</div>';


        google.maps.event.addListener(marker, 'click', function() {
            infowindow.close();
            map.setCenter(marker.getPosition());
            if(map.getZoom() < 10){
                map.setZoom(10);
            }
            infowindow.setContent(contentString);
            infowindow.open(map, marker);
        });

    })

    map.setCenter(new google.maps.LatLng(hotels[0].latitude, hotels[0].longitude));
    map.setZoom(10);

}

function initialize() {


    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(-34.397, 150.644),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
}

function loadScript() {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.googleapis.com/maps/api/js?key=" +
        googleMapsApiKey +
        "&sensor=" +
        googleMapsApiSensor +
        "&callback=initialize";
    document.body.appendChild(script);
}

hotelService = {}

hotelService.getHotels = function(begin, end, city){

    if(begin == "" || end == "" || city == ""){
        return;
    }

    var url = "/hotels/"+begin+"/"+end+"/"+city;

    $("#loading-indicator").show();

    $.getJSON( url, function( data ) {
        printMarkers(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown){
        alertError(jqXHR.responseText);
     })
    .always(function() {
        $("#loading-indicator").hide();
    });
}

hotelService.showModal = function(hotelId){

    var url = "/hotel/"+hotelId;

    $.getJSON( url, function( data ) {

        $("#hotel-modal #modal-header").empty();

        $("#hotel-modal #modal-header").append('<h3>'+data.name+'</h3>');

        $("#hotel-modal #book-button").empty();
        $("#hotel-modal #book-button").append('<a class="btn btn-primary pull-right"' +
            ' target="_blank" href="'+data.bookingUrl+'">Book now</a>');


        $("#hotel-modal .carousel-inner").empty();

        for(var i=0; i< data.imageUrls.length;i++){
            var imageUrl = data.imageUrls[i];

            var active = "";
            if(i == 0){
                active = "active";
            }
            var imageDiv =
                '<div class="item '+active+'">' +
                    '<img src="'+imageUrl+'"/>' +
                    '<div class="carousel-caption">' +
                    '</div>' +
                    '</div>'

            $("#hotel-modal .carousel-inner").append(imageDiv);
        }

        $("#hotel-modal #hotel-info").empty();

        var infoContent = '<dl class="dl-horizontal">' +
            '<dt>Name</dt>' +
            '<dd>'+data.name+'</dd>' +
            '<dt>Price</dt>' +
            '<dd>'+data.price+'</dd>' +
            '<dt>Check in</dt>' +
            '<dd>'+data.checkIn+'</dd>' +
            '<dt>Check out</dt>' +
            '<dd>'+data.checkOut+'</dd>' +
            '<dt>Description</dt>' +
            '<dd>'+data.description+'</dd>'+
            '<dt>Amenities</dt>' +
            '<dd>' +
            '<ul class="unstyled">';

        for(var i=0; i< data.facilities.length; i++){
            var facility = data.facilities[i];
            infoContent = infoContent + '<li>'+facility+'</li>'
        }
        infoContent = infoContent + '</ul>'+
            '</dd>' +
            '</dl>';


        $("#hotel-modal #hotel-info").append(infoContent);

        $("#hotel-modal").modal("show");
    }).fail(function(jqXHR, textStatus, errorThrown){
         alertError(jqXHR.responseText);
    });
}

function alertError(text){
    $('#alertBox #alertText').empty();
    $('#alertBox #alertText').append(text);

    $( "#alertBox" ).fadeIn( "slow", function() {});

    window.setTimeout(function() {
        $( "#alertBox" ).fadeOut( "slow", function() {});
    }, 5000);
}